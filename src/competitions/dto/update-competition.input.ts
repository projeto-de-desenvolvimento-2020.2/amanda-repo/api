import { CreateCompetitionInput } from './create-competition.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';
import { FilterableField } from '@nestjs-query/query-graphql';

@InputType()
export class UpdateCompetitionInput extends PartialType(CreateCompetitionInput) {
  @FilterableField({nullable: true})
  id: string;
  // @Field({nullable: true})
  // name: string;
  
  // @Field({nullable: true})
  // season: number;

  
  // @Field({nullable: true})
  // date: string;

  
  // @Field({nullable: true})
  // place: string;

  
  // @Field({nullable: true})
  // weight: string;
}
