import { NestjsQueryGraphQLModule, PagingStrategies } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import { Module } from '@nestjs/common';
import { CreateWorkoutTypeInput } from './dto/create-workout-type.input';
import { UpdateWorkoutTypeInput } from './dto/update-workout-type.input';
import { WorkoutType } from './entities/workout-type.entity';

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([WorkoutType])],
      resolvers: [
        {
          EntityClass: WorkoutType,
          DTOClass: WorkoutType,
          CreateDTOClass: CreateWorkoutTypeInput,
          UpdateDTOClass: UpdateWorkoutTypeInput,
          enableAggregate: true,
          pagingStrategy: PagingStrategies.NONE,
        }
      ]
    })
  ],
  providers: []
})
export class WorkoutTypeModule {}
